﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Yugamiru
{
    public class MuscleActionScriptMgr
    {

        SymbolFunc m_SymbolFunc = new SymbolFunc();
        int m_iCount;
        MuscleActionScriptElement[] m_pElement;
        // ‹Ø“÷ƒAƒNƒVƒ‡ƒ“ƒXƒNƒŠƒvƒgƒ}ƒXƒ^[ƒf[ƒ^.
        public MuscleActionScriptMgr()
        {

            m_iCount = 0;

            m_pElement = null;
    
    }

   
public bool ReadFromFile( string lpszFolderName, string lpszFileName, string pchErrorFilePath /* = NULL */ )
{
    char[] buf = new char[1024];
    List<MuscleActionScriptElement> listElement = new List<MuscleActionScriptElement>();

    if (m_pElement != null)
    {
        
        m_pElement = null;
    }
    m_iCount = 0;

            DualSourceTextReader DualSourceTextReader = new DualSourceTextReader();
            int i = DualSourceTextReader.Open(lpszFolderName, lpszFileName);
            if (i == 0)
            {
                return false;
            }

            string strError = string.Empty;
            int iLineNo = 1;
            while (!(DualSourceTextReader.IsEOF()))
            {
                buf[0] = '\0';
                DualSourceTextReader.Read(ref buf, 1024);
                //var t = File.ReadAllLines(@"Resources\ResultAction.inf");
                MuscleActionScriptElement Element = new MuscleActionScriptElement();

                int iErrorCode = Element.ReadFromString(ref buf);
                if (iErrorCode == Constants.READFROMSTRING_SYNTAX_OK)
                {
                    //listElement.push_back(Element);
                    listElement.Add(Element);
                }
                else
                {
                    m_SymbolFunc.OutputReadErrorString(ref strError, iLineNo, iErrorCode);
                }
                iLineNo++;
            }


            if (pchErrorFilePath != null)
    {
                StreamWriter fp = new StreamWriter(pchErrorFilePath);
                fp.Dispose();
            }

    // €–Ú”•ª‚¾‚¯ƒƒ‚ƒŠŠm•Û.
    int iSize = listElement.Count;
    if (iSize <= 0)
    {
        return true;
    }
    m_pElement = new MuscleActionScriptElement[iSize];
    if (m_pElement == null)
    {
        return false;
    }
    m_iCount = iSize;

            for (int j = 0; j < listElement.Count; j++)
            {
                m_pElement[j] = listElement[j];
            }

            return true;
}

public bool ResolveRange(  IntegerRangeDefinitionMgr RangeDefinitionMgr, string pchErrorFilePath /* = NULL */ )
{
    bool bRet = true;
    string strError = string.Empty;
    int i = 0;
    for (i = 0; i < m_iCount; i++)
    {
        if (m_pElement[i].m_iScriptOpecodeID == Constants.MUSCLESCRIPTOPECODEID_CHECK)
        {
            string strRangeSymbol = m_pElement[i].m_strRangeSymbol;
            int iMinValueOperatorID = 0;
            int iMaxValueOperatorID = 0;
            int iMinValue = 0;
            int iMaxValue = 0;
            if (RangeDefinitionMgr.GetRange(ref iMinValueOperatorID, ref iMaxValueOperatorID, ref iMinValue, ref iMaxValue, strRangeSymbol))
            {
                m_pElement[i].ResolveRange( iMinValueOperatorID,  iMaxValueOperatorID,  iMinValue,  iMaxValue);
            }
            else
            {
                string strTmp;
                strTmp = "Symbol "+ strRangeSymbol + " undefined\n";
                strError += strTmp;
                bRet = false;
            }
        }
    }
    if (pchErrorFilePath != null)
    {
                StreamWriter fp = new StreamWriter(pchErrorFilePath);
                fp.Dispose();
            }
    return bRet;
}

public bool Execute(MuscleColorInfo MuscleColorInfo, MuscleStateInfo MuscleStateInfo ) 
{
	bool bContextFlag = true;
MuscleColorInfo.Clear();
	int i = 0;
	for( i = 0; i<m_iCount; i++ ){
		if ( m_pElement[i].m_iScriptOpecodeID == Constants.MUSCLESCRIPTOPECODEID_START ){
			bContextFlag = true;
		}
		else if ( m_pElement[i].m_iScriptOpecodeID == Constants.MUSCLESCRIPTOPECODEID_CHECK ){
			int iMuscleID = m_pElement[i].m_iMuscleID;
int iCollectOperatorID = m_pElement[i].m_iCollectOperatorID;
int iMuscleStateTypeID = m_pElement[i].m_iMuscleStateTypeID;
double dValue = MuscleStateInfo.ApplyOperator(iCollectOperatorID, iMuscleID, iMuscleStateTypeID);
			if ( !(m_pElement[i].IsSatisfied(dValue ) ) ){
				bContextFlag = false;
			}
		}
		else if ( m_pElement[i].m_iScriptOpecodeID == Constants.MUSCLESCRIPTOPECODEID_END ){
			if ( bContextFlag ){
				int iColoredMuscleID = m_pElement[i].m_iColoredMuscleID;
int iMuscleColorID = m_pElement[i].m_iMuscleColorID;
MuscleColorInfo.AddData( iColoredMuscleID, iMuscleColorID );
			}
		}
	}
	return true;
}

    }
}
