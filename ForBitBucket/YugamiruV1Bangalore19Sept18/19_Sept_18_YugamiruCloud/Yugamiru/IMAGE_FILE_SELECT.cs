﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yugamiru
{
    public partial class IMAGE_FILE_SELECT : Form
    {
        JointEditDoc m_JointEditDoc;
        public IMAGE_FILE_SELECT(JointEditDoc GetDocument)
        {
            InitializeComponent();
          
                SystemImageList.SetTVImageList(treeView1.Handle);
                LoadRootNodes();
            m_JointEditDoc = GetDocument;

        }
        /// <summary>
        /// Loads the root TreeView nodes.
        /// </summary>
        private void LoadRootNodes()
        {
            // Create the root shell item.
            ShellItem m_shDesktop = new ShellItem();

            // Create the root node.
            TreeNode tvwRoot = new TreeNode();
            tvwRoot.Text = m_shDesktop.DisplayName;
            tvwRoot.Name = m_shDesktop.Path;
            tvwRoot.ImageIndex = m_shDesktop.IconIndex;
            tvwRoot.SelectedImageIndex = m_shDesktop.IconIndex;
            tvwRoot.Tag = m_shDesktop;


            // Now we need to add any children to the root node.
            ArrayList arrChildren = m_shDesktop.GetSubFolders();
            foreach (ShellItem shChild in arrChildren)
            {
                TreeNode tvwChild = new TreeNode();
                tvwChild.Text = shChild.DisplayName;
                tvwChild.Name = shChild.Path;
                tvwChild.ImageIndex = shChild.IconIndex;
                tvwChild.SelectedImageIndex = shChild.IconIndex;
                tvwChild.Tag = shChild;

               
                // If this is a folder item and has children then add a place holder node.
                if (shChild.IsFolder && shChild.HasSubFolder)
                    tvwChild.Nodes.Add("PH");
                tvwRoot.Nodes.Add(tvwChild);

            }

            // Add the root node to the tree.
            treeView1.Nodes.Clear();
            treeView1.SelectedNode = tvwRoot;
            treeView1.Focus();
            treeView1.Nodes.Add(tvwRoot);
            tvwRoot.Expand();
        }


        private void IMAGE_FILE_SELECT_Load(object sender, EventArgs e)
        {

        }

        private void treeView1_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            // Remove the placeholder node.
            e.Node.Nodes.Clear();
            try
            {
                // We stored the ShellItem object in the node's Tag property - hah!
                ShellItem shNode = (ShellItem)e.Node.Tag;
              
                    ArrayList arrSub = shNode.GetSubFolders();
              
                foreach (ShellItem shChild in arrSub)
                {
                    TreeNode tvwChild = new TreeNode();
                    tvwChild.Text = shChild.DisplayName;
                    tvwChild.Name = shChild.Path;
                    tvwChild.ImageIndex = shChild.IconIndex;
                    tvwChild.SelectedImageIndex = shChild.IconIndex;
                    tvwChild.Tag = shChild;

                    // If this is a folder item and has children then add a place holder node.
                    if (shChild.IsFolder && shChild.HasSubFolder)
                        tvwChild.Nodes.Add("PH");
                    e.Node.Nodes.Add(tvwChild);
                }
            }
            catch(Exception exc)
            {
                return;
            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Tag == null || e.Node.Name == "") return;
            PopulateListView(new DirectoryInfo(e.Node.Name));
        }
        #region Update folder list asynchronously
        private void PopulateListView(DirectoryInfo path)
        {
            imageListView1.Items.Clear();
            imageListView1.SuspendLayout();

            //int i = 0;
            try
            {
                foreach (FileInfo p in path.GetFiles("*.*"))
                {
                    if (p.Name.EndsWith(".jpg", StringComparison.OrdinalIgnoreCase) ||
                        p.Name.EndsWith(".png", StringComparison.OrdinalIgnoreCase) ||
                        p.Name.EndsWith(".bmp", StringComparison.OrdinalIgnoreCase) ||
                        p.Name.EndsWith(".ico", StringComparison.OrdinalIgnoreCase) ||
                        p.Name.EndsWith(".cur", StringComparison.OrdinalIgnoreCase) ||
                        p.Name.EndsWith(".emf", StringComparison.OrdinalIgnoreCase) ||
                        p.Name.EndsWith(".wmf", StringComparison.OrdinalIgnoreCase) ||
                        p.Name.EndsWith(".tif", StringComparison.OrdinalIgnoreCase) ||
                        p.Name.EndsWith(".tiff", StringComparison.OrdinalIgnoreCase) ||
                        p.Name.EndsWith(".gif", StringComparison.OrdinalIgnoreCase))
                    {
                        imageListView1.Items.Add(p.FullName);
                        //if (i == 1) imageListView1.Items[imageListView1.Items.Count - 1].Enabled = false;
                        //i++;
                        //if (i == 3) i = 0;
                    }
                }
            }
            catch(Exception e)
            {
                //throw new IOException("The device is not ready.");
                return;

            }
            imageListView1.ResumeLayout();
        }
        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            var image = imageListView1.SelectedItems;
            if (imageListView1.SelectedItems.Count > 0)
            {
                var item = imageListView1.SelectedItems[0].FileName;
                m_JointEditDoc.m_Image_Path = item;
                this.DialogResult = DialogResult.OK;
            }
            this.Close();
        
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            treeView1.Dispose();
            this.Close();
            

        }
    }
}
